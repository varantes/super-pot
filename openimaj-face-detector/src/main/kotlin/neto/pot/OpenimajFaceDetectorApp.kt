package neto.pot

import org.openimaj.image.DisplayUtilities
import org.openimaj.image.ImageUtilities
import org.openimaj.image.MBFImage
import org.openimaj.image.colour.RGBColour
import org.openimaj.image.colour.Transforms
import org.openimaj.image.processing.face.detection.HaarCascadeDetector
import org.openimaj.image.processing.face.detection.keypoints.FKEFaceDetector
import org.slf4j.LoggerFactory
import java.io.File
import java.net.URL

class OpenimajFaceDetectorApp {

    private val log = LoggerFactory.getLogger(javaClass)

    fun markFace() {
        log.info("oi")
        val image = ImageUtilities.readMBF(File("fotos/murilo_1.jpg"))
        HaarCascadeDetector(40).detectFaces(Transforms.calculateIntensity(image)).forEach { face ->
            image.drawShape(face.bounds, RGBColour.RED)
        }
        DisplayUtilities.display(image)
        println("fim")
    }

    fun cropFace(imgFile: File) = cropFace(ImageUtilities.readMBF(imgFile))

    fun cropFace(image: MBFImage) {
        FKEFaceDetector(40).detectFaces(Transforms.calculateIntensity(image)).forEach { face ->
            //image.drawShape(face.bounds, RGBColour.RED)
            DisplayUtilities.display(image.extractROI(face.bounds.calculateRegularBoundingBox()), "cropped")
        }
        DisplayUtilities.display(image, "original")
    }

    fun cropFace1(imgFile: File) {
        val image = ImageUtilities.readMBF(imgFile)
        val faces = FKEFaceDetector(40).detectFaces(Transforms.calculateIntensity(image))
        DisplayUtilities.display("images", image, image.extractROI(faces[0].bounds.calculateRegularBoundingBox()))
    }

    fun displayWebPhoto() {
        val image = ImageUtilities.readMBF(URL("https://upload.wikimedia.org/wikipedia/commons/thumb/5/5b/Ghost_In_The_Shell_World_Premiere_Red_Carpet-Scarlett_Johansson_%2837147912300%29.jpg/250px-Ghost_In_The_Shell_World_Premiere_Red_Carpet-Scarlett_Johansson_%2837147912300%29.jpg"))
        DisplayUtilities.display(image)
    }
}

fun main() = OpenimajFaceDetectorApp().
        //cropFace(File("fotos/murilo_1.jpg"))
        cropFace(ImageUtilities.readMBF(URL("https://upload.wikimedia.org/wikipedia/commons/thumb/5/5b/Ghost_In_The_Shell_World_Premiere_Red_Carpet-Scarlett_Johansson_%2837147912300%29.jpg/250px-Ghost_In_The_Shell_World_Premiere_Red_Carpet-Scarlett_Johansson_%2837147912300%29.jpg")))
//displayWebPhoto()